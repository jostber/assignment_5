<?php

class Club {
	public $id;
	public $clubName;
	public $city;
	public $country;


	public function __construct($id, $clubName, $city, $country)
    {
      $this->id = $id;
      $this->clubName = $clubName;
	    $this->city = $city;
	    $this->country = $country;
    }
}
?>
