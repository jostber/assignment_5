<?php

class Logbook {
	public $season;
	public $skierUserName;
	public $clubID;
	public $totalDistance;


	public function __construct($season, $skierUserName, $clubID, $totalDistance)
    {
      $this->season = $season;
      $this->skierUserName = $skierUserName;
	    $this->clubID = $clubID;
	    $this->totalDistance = $totalDistance;
    }
}
?>
