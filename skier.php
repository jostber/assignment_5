<?php

class Skier {
	public $userName;
	public $firstName;
	public $lastName;
	public $DOB;


	public function __construct($userName, $firstName, $lastName, $DOB)
    {
      $this->userName = $userName;
      $this->firstName = $firstName;
	    $this->lastName = $lastName;
	    $this->DOB = $DOB;
    }
}
?>
