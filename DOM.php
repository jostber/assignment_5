<?php

include_once("club.php");
include_once("skier.php");
include_once("logbook.php");

function insertSkier($PDO, $doc) {

$XPath = new DOMXpath($doc);
$elements = $XPath->query("/SkierLogs/Skiers/Skier");


  foreach($elements as $element) {
      $userName = $element->getAttribute("userName");
      $firstName = $element->childNodes->item(1)->nodeValue;
      $lastName = $element->childNodes->item(3)->nodeValue;
      $DOB = $element->childNodes->item(5)->nodeValue;

      $PDO->addskier(new skier($userName, $firstName, $lastName, $DOB));
    }
}

function insertClub($PDO, $doc) {

$XPath = new DOMXpath($doc);
$elements = $XPath->query("/SkierLogs/Clubs/Club");

  foreach($elements as $element) {
      $id = $element->getAttribute("id");
      $clubName = $element->childNodes->item(1)->nodeValue;
      $city = $element->childNodes->item(3)->nodeValue;
      $country = $element->childNodes->item(5)->nodeValue;

      $PDO->addClub(new club($id, $clubName, $city, $country));
    }
}

function insertLogbook($PDO, $doc) {

$XPath = new DOMXpath($doc);
$elements = $XPath->query("/SkierLogs/Season");

  foreach($elements as $element) {
      $season = $element->getAttribute("fallYear");
      //echo $season;
      foreach($XPath->query("/SkierLogs/Season[@fallYear=$season]/Skiers") as $element) {
        if($element->hasAttribute('clubId')){

          $clubID = $element->getAttribute("clubId");
          foreach($XPath->query("/SkierLogs/Season[@fallYear=$season]/Skiers[@clubId=\"$clubID\"]/Skier")as $element){
          $skierUserName = $element->getAttribute("userName");
          $totalDistance = 0;
            foreach($XPath->query("/SkierLogs/Season[@fallYear=$season]/Skiers[@clubId=\"$clubID\"]/Skier[@userName=\"$skierUserName\"]/Log/Entry/Distance") as $elements) {
              $totalDistance+=$elements->nodeValue;
            }
            $PDO->addLogbook(new Logbook($season, $skierUserName, $clubID, $totalDistance));
          }
        }
        else{
          $clubId = NULL;
          foreach($XPath->query("/SkierLogs/Season[@fallYear=$season]/Skiers[not(@clubId)]/Skier")as $element){
          $skierUserName = $element->getAttribute("userName");
          $totalDistance = 0;
            foreach($XPath->query("/SkierLogs/Season[@fallYear=$season]/Skiers[not(@clubId)]/Skier[@userName=\"$skierUserName\"]/Log/Entry/Distance") as $elements) {
              $totalDistance+=$elements->nodeValue;
            }
            $PDO->addLogbook(new Logbook($season, $skierUserName, $clubId, $totalDistance));
        }
        }
      }
    }
}

?>
