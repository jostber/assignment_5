<?php
include_once("club.php");
include_once("logbook.php");
include_once("skier.php");

class DBModel {

    protected $db = null;
    public function __construct($db = null) {
	    if ($db) {
      $this->db = $db;
      echo "Could not connect to the database";
		}
    else {
          try {                                         // Endret til assignment5
            $this->db = new PDO('mysql:host=localhost; dbname=assignment5; charset=utf8', 'root', '');
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            echo "Connected successfully". "\n";
        }

         catch(PDOException $e) {
         echo "Connection failed: " . $e->getMessage();
         }
		   }
    }

    public function addSkier($skier) {


            $stmt = $this->db->prepare("INSERT INTO skier(userName, firstName, lastName, DOB) VALUES (:userName, :firstName, :lastName, :DOB)");
            $stmt->bindValue(':userName', $skier->userName);
            $stmt->bindValue(':firstName', $skier->firstName);
            $stmt->bindValue(':lastName', $skier->lastName);
            $stmt->bindValue(':DOB', $skier->DOB);
            $stmt->execute();
    }

    public function addClub($club) {

            $stmt = $this->db->prepare("INSERT INTO club(id, clubName, city, country) VALUES (:id, :clubName, :city, :country)");
            $stmt->bindValue(':id', $club->id);
            $stmt->bindValue(':clubName', $club->clubName);
            $stmt->bindValue(':city', $club->city);
            $stmt->bindValue(':country', $club->country);
            $stmt->execute();
    }

    public function addLogbook($logbook) {

            $stmt = $this->db->prepare("INSERT INTO logbook(season, skierUserName, clubID, totalDistance) VALUES (:season, :skierUserName, :clubID, :totalDistance)");
            $stmt->bindValue(':season', $logbook->season);
            $stmt->bindValue(':skierUserName', $logbook->skierUserName);
            $stmt->bindValue(':clubID', $logbook->clubID);
            $stmt->bindValue(':totalDistance', $logbook->totalDistance);
            $stmt->execute();
    }
}

?>
