<?php

include_once("DBModel.php");
include_once("DOM.php");

$start = new DBModel();
$doc = new DOMDocument();
$doc->load("SkierLogs.xml");

if($doc) {
  echo "SkierLogs.xml is loaded into the system.";
}
insertSkier($start, $doc);
insertClub($start, $doc);
insertLogbook($start, $doc);
?>
